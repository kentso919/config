# Personalized config files for vim, zsh, etc ...
1. Install vim-plug
2. Install vim plugins

    vim +PluginInstall +qa

3. Set vim as default editor

    update-alternatives --config editor
