#!/bin/bash

#Install c, c++ compiler and Make
#apt-get install -y aptitde
sudo add-apt-repository -y ppa:chris-lea/node.js
sudo aptitude update
sudo aptitude install -y build-essential
sudo aptitude install -y git

#Install vim
sudo aptitude install -y vim-nox
wget www.cse.cuhk.edu.hk/~kytso/vim/.vimrc -O ~/.vimrc

git clone https://github.com/gmarik/vundle.git ~/.vim/bundle/vundle
vim +BundleInstall +qa

#Install node.js
sudo aptitude install -y python-software-properties python g++ make
sudo aptitude install -y nodejs

#Install zsh
sudo aptitude install -y zsh
curl -L https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh | sh
chsh -s /bin/zsh

#Install ssh server
sudo aptitude install -y openssh-server

#Install tmux
sudo aptitude install -y tmux

#Install fail2ban
#sudo aptitude install -y fail2ban

#Setup git
git config --global user.name "Ken Tso"
#git config --global user.email ""
git config --global alias.st status
git config --global color.ui true

#Manual Setup
#update-alternatives --config editor
# ZSH_THEME="blinks"
# plugins=(git node npm python cp rsync)
# alias tmux='tmux -2'

#For Mac OSX mosh
# export LANG=en_US.UTF-8
# export LC_ALL=en_US.UTF-8
